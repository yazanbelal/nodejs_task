import {Csv} from "./csv";
import {Author} from "../models/author";

type AuthorRow = {
    email: string,
    firstname: string,
    lastname: string,
}

export class AuthorCsv extends Csv {

    createOrUpdateRow(row: AuthorRow) {
        Author.upsert({
            email: row.email,
            firstname: row.firstname,
            lastname: row.lastname
        }).catch((reason) => {
            //log errors
            console.log(reason)
        });
    }

}