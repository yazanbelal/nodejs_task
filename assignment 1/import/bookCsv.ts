import {Csv} from "./csv";
import {Book} from "../models/book";
import {Author} from "../models/author";
import {sequelize} from "../db";

type BookRow = {
    title: string,
    isbn: string,
    authors: string,
    description: string
}

export class BookCsv extends Csv {

    createOrUpdateRow(row: BookRow) {
        Book.create({
            title: row.title,
            isbn: row.isbn,
            description: row.description,
        }).then(async (book) => {
            let authors: string[] = row.authors.split(',');
            for (let i = 0; i < authors.length; i++) {
                let authorEmail = authors[i];
                let author = await Author.findOne({where: {email: authorEmail}});

                await sequelize.model('AuthorBooks').create({
                    AuthorId: author?.getDataValue('id'),
                    BookId: book.getDataValue('id')
                })
            }
        });
    }

}