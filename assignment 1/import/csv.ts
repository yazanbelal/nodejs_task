export abstract class Csv {
    separator: string = ';'

    abstract createOrUpdateRow(row: any): void;
}
