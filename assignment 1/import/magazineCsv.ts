import {Csv} from "./csv";
import {Author} from "../models/author";
import {sequelize} from "../db";
import moment from "moment";
import {Magazine} from "../models/magazine";

type MagazineRow = {
    title: string,
    isbn: string,
    authors: string,
    publishedAt: string
}

export class MagazineCsv extends Csv {
    createOrUpdateRow(row: MagazineRow) {
        let date = moment(row.publishedAt, 'DD.MM.YYYY')
        Magazine.create({
            title: row.title,
            isbn: row.isbn,
            publishedAt: date,
        }).then(async (magazine) => {
            let authors: string[] = row.authors.split(',');
            for (let i = 0; i < authors.length; i++){
                let authorEmail = authors[i];
                let author = await Author.findOne({where: {email: authorEmail}});

                await sequelize.model('AuthorMagazines').create({
                    AuthorId: author?.getDataValue('id'),
                    MagazineId: magazine.getDataValue('id')
                })
            }
        });
    }

}