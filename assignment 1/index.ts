import {sequelize} from "./db";
import {Book} from "./models/book";
import {Author} from "./models/author";
import {Magazine} from "./models/magazine";
import express, {Express, Request, Response} from 'express';
import multer from "multer";
import {ImportQueue} from "./queue";

/**
 * Multer file upload middleware
 */
const upload = multer({dest: 'uploads/'})

/**
 * sequelize relationships
 */
Book.belongsToMany(Author, {through: 'AuthorBooks'});
Magazine.belongsToMany(Author, {through: 'AuthorMagazines'});

/**
 * force rebuild sequelize database table
 */
(async () => {
    await sequelize.sync({force: true});
})();

const app: Express = express();
const port = 3000;

/**
 * Main route with upload form
 */
app.get('/', async (req: Request, res: Response) => {
    res.sendFile(__dirname + '/views/index.html');
});

/**
 * upload csv route
 */
app.post('/uploadCsv', upload.any(), async (req: Request, res: Response) => {
    if (req.files == undefined){
        return res.send('No files')
    }

    let files: any[] = [];
    let fileKeys = Object.keys(req.files);

    fileKeys.forEach((key) => {
        // had to ts-ignore this part because I was getting type hint error for some reason
        // @ts-ignore
        files.push(req.files[key]);
    });

    /**
     * used to only get files with text/csv mimetype to not handle other uploaded files
     */
    let mimetype: string = 'text/csv'

    /**
     * get author file from the files array
     */
    let authorFile = files.filter((val, i) => {
        return val.fieldname == 'authors' && val.mimetype == mimetype
    })
    /**
     * get book file from the files array
     */
    let bookFile = files.filter((val, i) => {
        return val.fieldname == 'books' && val.mimetype == mimetype
    })
    /**
     * get magazine file from the files array
     */
    let magazineFile = files.filter((val, i) => {
        return val.fieldname == 'magazines' && val.mimetype == mimetype
    })

    /**
     * only allow upload of magazines and books if there's author files first since it relies on their relationship
     */
    if (bookFile && magazineFile && authorFile == undefined){
        return res.send("Can't upload books and magazines without authors")
    }

    if (authorFile){
        ImportQueue.add({
            file: authorFile[0],
            type: "author"
        })
    }
    if (bookFile){
        ImportQueue.add({
            file: bookFile[0],
            type: "book"
        })
    }
    if (magazineFile){
        ImportQueue.add({
            file: magazineFile[0],
            type: "magazine"
        })
    }

    res.send("Importing, <a href='/'>Go Back</a>")
});


app.get('/books/:isbn', async (req: Request, res: Response) => {
    let book = await Book.findOne({
        where: {
            isbn: req.params.isbn
        }
    })

    res.send(book?.toJSON() ?? 'Not Found')
})

app.get('/magazines/:isbn', async (req: Request, res: Response) => {
    let magazine = await Magazine.findOne({
        where: {
            isbn: req.params.isbn
        }
    })

    res.send(magazine?.toJSON() ?? 'Not Found')
})

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`)
});