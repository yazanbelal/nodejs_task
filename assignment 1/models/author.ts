import {DataTypes} from "sequelize";
import {sequelize} from "../db";

export const Author = sequelize.define('Author', {
    email: {
        type: DataTypes.STRING,
        unique: true
    },
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING
});