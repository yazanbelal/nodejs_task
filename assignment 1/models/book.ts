import {DataTypes} from "sequelize";
import {sequelize} from "../db";

export const Book = sequelize.define('Book', {
    title: DataTypes.STRING,
    isbn: {
        type: DataTypes.STRING,
        unique: true
    },
    description: DataTypes.TEXT
});
