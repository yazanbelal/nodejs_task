import {DataTypes} from "sequelize";
import {sequelize} from "../db";

export const Magazine = sequelize.define('Magazine', {
    title: DataTypes.STRING,
    isbn: {
        type: DataTypes.STRING,
        unique: true
    },
    publishedAt: DataTypes.DATE
});