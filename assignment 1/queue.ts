import Queue, {Job} from 'bull';
import parse from "csv-parser";
import fs from "fs"
import {BookCsv} from "./import/bookCsv";
import {AuthorCsv} from "./import/authorCsv";
import {MagazineCsv} from "./import/magazineCsv";


//todo: use .env variables for redis (dotenv library)
export const ImportQueue = new Queue('importing csv', 'redis://127.0.0.1:6379');

/**
 * Import queue to handle background file read of csv files and importing to the database
 */
ImportQueue.process(async (job: Job, done) => {

    let path: string = job.data.file.path;
    let separator = ';'

    fs.createReadStream(path)
        .pipe(parse({separator: separator}))
        .on("data", async (row: any) => {
            if (job.data.type == 'author') {
                let authorCsv = new AuthorCsv();
                authorCsv.createOrUpdateRow(row)
            } else if (job.data.type == 'book') {
                let bookCsv = new BookCsv()
                bookCsv.createOrUpdateRow(row)
            } else if (job.data.type == 'magazine') {
                let magazineCsv = new MagazineCsv()
                magazineCsv.createOrUpdateRow(row)
            }
        });
    done();
});