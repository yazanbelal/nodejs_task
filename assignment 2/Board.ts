/**
 * Board class to handle the board logic
 */
export class Board {
    private readonly size: number;
    board: Array<Array<number>>;

    /**
     * Board class constructor with custom size
     * @param size
     */
    constructor(size: number = 8) {
        this.size = size;
        this.board = new Array(this.size);
        this.initializeBoard();
    }

    /**
     * board initialize function to fill initialized board array with default 0 values
     * @private
     */
    private initializeBoard() {
        for (let i = 0; i < this.size; i++) {
            this.board[i] = new Array(this.size);
            for (let j = 0; j < this.size; j++) {
                this.board[i][j] = 0;
            }
        }
    }

    /**
     * check if x and y positions are inside the board or not
     * @param x
     * @param y
     */
    inBoard(x: number, y: number): boolean {
        return !(x < 0 || x >= this.size || y < 0 || y >= this.size);
    }

    /**
     * change an x and y position in the board to 1 (filled value)
     * @param x
     * @param y
     */
    changeValue(x: number, y: number) {
        this.board[x][y] = 1;
    }

    /**
     * pretty print the board for cli
     */
    printBoard() {
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                process.stdout.write(this.board[i][j] + " ")
            }
            process.stdout.write("\n")
        }
    }
}