import {Board} from "./Board";
import {PieceInterface} from "./Pieces/PieceInterface";


/**
 * Game class to handle main game process
 */
export class Game {
    private readonly board: Board;

    constructor(board: Board) {
        this.board = board;
    }

    /**
     * Marks possible moves per piece on the board
     * @param piece
     */
    markPossibleMoves(piece: PieceInterface){
        piece.possibleMoves().forEach((move) => {
            if(this.board.inBoard(move[0], move[1])){
                this.board.changeValue(move[0], move[1]);
            }
        });

        return this.board;
    }
}