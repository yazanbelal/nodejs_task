import {PieceInterface} from "./PieceInterface";

/**
 * Chess Knight Class
 */
export class Knight implements PieceInterface{
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    /**
     * Get knight possible moves from given x and y
     */
    possibleMoves(): number[][] {
        let steps = [
            [1, 2], [-1, 2], [1, -2], [-1, -2], [2, 1], [-2, 1], [2, -1], [-2, -1]
        ];

        let moves: number[][] = [];

        steps.forEach((step) => {
            moves.push([this.x + step[0], this.y + step[1]]);
        });

        return moves;
    }
}