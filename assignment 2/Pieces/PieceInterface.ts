/**
 * Chess piece interface with common things shared between chess pieces
 */
export interface PieceInterface {
    x: number;
    y: number;

    possibleMoves(): number[][];
}