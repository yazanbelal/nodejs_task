import "./Board"
import {Board} from "./Board";
import {Game} from "./Game";
import {Knight} from "./Pieces/Knight";

const boardSize = 8;

let args = process.argv.slice(2);
let x = parseInt(args[0]);
let y = parseInt(args[1]);

x--;
y--;

if (isNaN(x) || isNaN(y) ){
    process.stdout.write("Please provide position (x, y)\nex: node main.js 7 1\n");
    process.exit(0);
}

if (x > boardSize || x < 0 || y > boardSize || y < 0){
    process.stdout.write("x and y position has to be between 0 and the board size");
    process.exit(0);
}

let board = new Board(boardSize);
let knight = new Knight(x, y);
let game = new Game(board);

game.markPossibleMoves(knight);

board.printBoard();
